# imports
import config
import sys
from profiles.mac import Mac
from subprocess import Popen, PIPE
import json

def main():
    scan_profile = find_OS(config.SCAN_PROFILE_MAP)
    p = scan_profile()
    data = p.scan()
    #print(data)

    #parsed = json.loads(data)
    print(json.dumps(data, indent=4, sort_keys=True))

    with open('data.txt', 'w') as outfile:  
        json.dump(data, outfile, indent=4)

    # After your system has been scanned correctly, and the output is normalized,
    #     print the final JSON here (just the JSON don't worry about the request) 

def find_OS(osList):
    for k in osList.keys():
        if k == "macOS":
            return osList[k]

if __name__ == '__main__':
    main()