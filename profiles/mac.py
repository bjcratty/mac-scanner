# OS profile template import would go here (no needed)
import re
import sys
import os
import subprocess
from subprocess import Popen, PIPE
import shlex
import string
import itertools
import ifcfg
import json
import cpuinfo

class Mac():
    def __init__(self):  # def __init__(self, device, credentials):
        print('Connecting to server...')

    @property
    def file_systems(self):
        file_system = {} #supposed to be #

        """
        ubuntu is df -P -BGB -x nfs
        """
        result = subprocess.run(['df','-Phl'], stdout=subprocess.PIPE)
        result = result.stdout.decode()
        result = result.strip().split('\n')[1:]
        for line in result:
            i = line.split()

            #file_system["FileSystem"] = {
            file_system["file_system"] = i[0]
            file_system["size (MB)"] = normalizeMemory(i[1][:-2],i[1][-2:])#i[1],
            file_system["used (MB)"] = normalizeMemory(i[2][:-2],i[2][-2:])#
            file_system["free (MB)"] = normalizeMemory(i[3][:-2],i[3][-2:])#
            file_system["capacity"] = i[4]
            file_system["MountLocation"] = i[5]
               # }
            
        return file_system


    @property
    def hardware(self):
        ## note the double ## means it was the old way I did it.
        ##hw = {}
        #Goal: Return the following information:
        # CPU Architecture
        # CPU Core Count 
        # CPU Socket Count 
        # CPU Model
        # CPU Speed (MHz)
        # Firmware version
        # Memory Installed (MB)
        # Model
        # Vendor
        # BIOS
        result = subprocess.run(['system_profiler','SPHardwareDataType'], stdout=subprocess.PIPE)
        result = result.stdout.decode()
        result = result.split("\n")

        ## info = cpuinfo.get_cpu_info()
        ## hw["CPU Architecture"] = info["arch"]
        ## hw["Cores"] = info["count"]
        ## hw["CPU Sockets"] = subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Number of Processors' | awk -F ':' '{print $2}'")[1:]
        ## hw["Model"] = info["brand"]
        ## hw["CPU Speed (GHz)"] = info["hz_advertised_raw"][0]/1000000
        ## hw["Firmware"] = subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'SMC' | awk -F ':' '{print $2}'")[1:]
        ## hw["Memory Installed"] = subprocess.getoutput("system_profiler SPHardwareDataType | grep Memory | awk -F ':' '{print $2}'")[1:]
        ## hw["CPU Model"] = info["brand"]
        ## hw["Vendor"] = info["vendor_id"]
        ## hw["BIOS"] = subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Boot' | awk -F ':' '{print $2}'")[1:]

        return {
            "CPU Architecture" : subprocess.getoutput("uname -a")[-2:],##info["arch"],
            "Cores" : getSystemInfo("Total Number of Cores"), #subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Total Number of Cores' | awk -F ':' '{print $2}'")[1:], #info["count"], 
            "CPU Sockets" : getSystemInfo("Number of Processors"), #subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Number of Processors' | awk -F ':' '{print $2}'")[1:],
            "Model" : getSystemInfo("Model Name"), #subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Model Name' | awk -F ':' '{print $2}'")[1:], #info["brand"]
            "CPU Speed (GHz)" : getSystemInfo("Processor Speed"), #float(subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Processor Speed' | awk -F ':' '{print $2}'")[1:4]) * 1000, #info["hz_advertised_raw"][0]/1000000 #use the other commands but its gonna need some more parsing and math
            "Firmware" : getSystemInfo("SMC"), #subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'SMC' | awk -F ':' '{print $2}'")[1:],
            "Memory Installed" : getSystemInfo("Memory"), #subprocess.getoutput("system_profiler SPHardwareDataType | grep Memory | awk -F ':' '{print $2}'")[1:],
            "CPU Model" : getSystemInfo("Processor Name"), #subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Processor Name' | awk -F ':' '{print $2}'")[1:], #info["brand"] #subprocess.getoutput("sysctl -n machdep.cpu.brand_string"),
            "CPU Vendor" : subprocess.getoutput("sysctl -n machdep.cpu.vendor"), #info["vendor_id"],
            "Device Vendor" : "Apple Inc.",
            "BIOS" : getSystemInfo("Boot") #subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Boot' | awk -F ':' '{print $2}'")[1:]
        }

        ## return hw


    # @property
    # def network_interfaces(self):
    #     # net_info = []
    #     # p1 = subprocess.Popen(['echo', "Welcome To The Geek Stuff!"], stdout=PIPE)
    #     # p2 = subprocess.Popen(['sed', 's/\(\b\)/\(\1\)/g'], stdin=p1.stdout, stdout=PIPE)
    #     # p1.stdout.close()
    #     # output = p2.communicate()
    #     #cline = "echo 'Welcome To The Geek Stuff' | sed 's/[A-Z]/(&)/g'"
    #     out = subprocess.getoutput("ifconfig -a")
    #     out = out.strip().split('\n')
    #     buffer = [out[0]]
    #     lst = []
    #     for line in out[1:]:
    #         if line == '':
    #             continue

    #         if line[0].isalpha():
    #             lst.append([e for e in buffer])
    #             buffer.clear()
    #             buffer.append(line)
    #         else:
    #             buffer.append(line)

    #         print(line)
    #     lst.append(buffer)


    #     data = [list(map(str.strip, e)) for e in lst]
    #     nic_information = []
    #     split_by_colon = lambda s: s.split(":")[1]

    #     for nic in data:
    #         if nic[0].startswith("lo") or not nic[1].startswith("inet"):
    #             continue

    #         temp = {"type": "EthernetInterface"}

    #         l = nic[0].split()
    #         temp['name'] = l[0]
    #         temp['mac_address'] = l[-1]

    #         l2 = nic[1].split()
    #         temp["ip_addresses"] = [
    #             {
    #                 "ip_address": split_by_colon(l2[1]),
    #                 "subnet": split_by_colon(l2[-1])
    #             }
    #         ]
    #         if nic[2].startswith("inet6"):
    #             l3 = nic[3].split()
    #         else:
    #             l3 = nic[2].split()
    #         temp['status'] = 'active' if l3[0]=='UP' else 'inactive'
    #         temp["mtu"] = int(split_by_colon(l3[-2]))

    #         try:
    #             print(l[0])
    #             #temp["duplex"] = 

    #         except:
    #             pass

    #         nic_information.append(temp)
    #     return nic_information

    @property
    def network_interfaces(self):
        # net_info = []
        #ifdata = ifparser.Ifcfg(subprocess.getoutput("ifconfig -a"))

        #Use the following line to return a nice little JSON object of all the data.
        #ifdata = subprocess.getoutput("python3 -m ifcfg.cli | python3 -mjson.tool")
        ifdata = subprocess.getoutput("python3 -m ifcfg.cli")
        ifdata = json.loads(ifdata)
        #print(ifdata)

        #print(ifdata.interfaces)
        # print(ifcfg.interfaces())
        # for name, interface in ifcfg.interfaces().items():
        #     # do something with interface
        #     print(interface['device'])
        #     print(interface['mtu'])      # First IPv4 found
        #     print(interface['status'])        # List of ips
        #     print(interface['inet'])
        #     print(interface['netmask'])
        #     print(interface['mac'])
        #     print(interface['duplex'])

        # default = ifcfg.default_interface()

        # for name, interface in ifcfg.interfaces().items():
        #     print(interface)
        #     print("\n")
        #parsed = json.loads(ifdata)
        #print(json.dumps(parsed, indent=4, sort_keys=True))
        #print(json.dumps(ifdata))
        return ifdata


    @property
    def operating_system(self):
        name = subprocess.run(['sw_vers','-productName'], stdout=subprocess.PIPE)
        version = subprocess.run(['sw_vers','-productVersion'], stdout=subprocess.PIPE)
        build = subprocess.run(['sw_vers','-buildVersion'], stdout=subprocess.PIPE)
        kernel = subprocess.run(['uname'], stdout=subprocess.PIPE)
        return {
            "name": name.stdout.decode().strip(),
            "version": version.stdout.decode().strip(),
            "build": build.stdout.decode().strip(),
            "kernel": kernel.stdout.decode().strip()
        }


    def scan(self):
        return {
            "file_systems": self.file_systems,
            "hardware": self.hardware,
            "network_interfaces": self.network_interfaces,
            "operating_system": self.operating_system
        }

def normalizeMemory(bits, unit):
        if unit == "Gi":
            return (float(bits)*10)
        elif unit == "Ki":
            return (float(bits)/10)
        else:
            return int(bits)

def runcmd(cmd, flag):
    result = subprocess.run([cmd,flag], stdout=subprocess.PIPE)
    return result

def getSystemInfo(flag):
    if flag == "Processor Speed":
        result = float(subprocess.getoutput("system_profiler SPHardwareDataType | grep -i 'Processor Speed' | awk -F ':' '{print $2}'")[1:4]) * 1000
    else:
        cmd = "system_profiler SPHardwareDataType | grep -i '" + flag + "' | awk -F ':' '{print $2}'"
        result = subprocess.getoutput(cmd)[1:]
    return result