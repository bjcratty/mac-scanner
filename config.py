# imports
from profiles.mac import Mac

SCAN_PROFILE_MAP = {
    "mac": Mac,
    "darwin": Mac,
    "macOS": Mac
}