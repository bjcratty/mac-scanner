# imports

def main():

    # --- 1. Determine the type of scan that will be ran

    # scan_type = "non_chlk_only" if len(sys.argv) < 2 else "custom"

    # --- 2. Prepare the request payload; add "custom_targets" if the scan is "custom"

    # payload = { "requestor": config.SCANNER, "scan_name": scan_type }
    # if scan_type == "custom":
    #     payload["custom_targets"] = ",".join(sys.argv[1:])

    # --- 3. Using a http get request the values for the URL, credentials, and payload will be saved 

    # response = requests.get(
    #     url=config.PRE_SCAN_URL,
    #     cert=config.CREDENTIALS,
    #     params=payload
    # )

    # --- 4. If the http request fails then log the error code and reason for failure

    # if not response.ok:
    #     LOGGER.error(f"HTTP {response.status_code} {response.reason}")
    #     sys.exit()

    # --- 5. HTTP request info data is "converted" to JSON and stored.
    # json_data = response.json()

    # --- 6. Create file workaround.json and write all credential information that we got from the HTTP request
    # with open("workaround.json", "w") as encrypted_credntials:
    #     encrypted_credntials.write(
    #         str(
    #             json.dumps(json_data["credentials"])
    #         )
    #     )

    # --- 7. Create a thread that compiles/runs/executes temp.rb. This is a simple read over the workaround.json file.
    #        the only difference is that the ruby program runs the Aescrypt library to decrypt the passwords and stores it in temp.json
    # subprocess.call("C:\\Ruby23-x64\\bin\\ruby.exe temp.rb", shell=False)

    # --- 8. Open and the temp.json file load it into our JSON from earlier.
    # with open("temp.json") as temp:
    #     decrypted_credentials = json.load(temp)

    # --- 9. Create our own mapping of the JSON by having ID by the key to each individual username and password.
    # credential_map = {
    #     c["id"]: {
    #         "username": c["username"],
    #         "password": c["password"]
    #     } for c in decrypted_credentials
    # }

    # --- 10.
    # At this point devices in `json_data['hitlist']` will be scanned
    
    # Import your `Mac` profile and "scan" your system

    # After your system has been scanned correctly, and the output is normalized,
    #     print the final JSON here (just the JSON don't worry about the request) 
    
    return 

if __name__ == '__main__':
    main()